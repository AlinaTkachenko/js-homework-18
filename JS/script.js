let user = {
     name: 'Alina',
     lastname: 'Tkachenko',
     age: 31,
     null: 'test',
     husband: {
          name: 'Andrey',
          lastname: 'Tkachenko',
          age: 32,
     }
}


function clone(obj) {
     
     let cloneObj = {};

     for (let key in obj) {
          if (typeof(obj[key]) != 'object' || key === null){
               cloneObj[key] = obj[key];
          }
          else{
               cloneObj[key] = clone(obj[key]);
          }
     }
     return cloneObj;
}


let user1 = clone(user);
console.log(user);
console.log(user1);

console.log(user.husband === user1.husband);
